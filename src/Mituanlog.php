<?php

namespace think;

class Mituanlog extends Log 
{
    public function save($force = false)
    {
        if (empty($this->log) || !$this->allowWrite) {
            return true;
        }

        if (!$this->check($this->config)) {
            // 检测日志写入权限
            return false;
        }

        if (!$force && is_array($this->log) && count($this->log, COUNT_RECURSIVE) < 100) {
            return true;
        }

        $log = [];

        foreach ($this->log as $level => $info) {
            if (!$this->app->isDebug() && 'debug' == $level) {
                continue;
            }

            if (empty($this->config['level']) || in_array($level, $this->config['level'])) {
                $log[$level] = $info;

                var_export("ready for record a line \n");

                $this->app['hook']->listen('log_level', [$level, $info]);
            }
        }

        var_export($log);

        $result = $this->driver->save($log, true);

        if ($result) {
            $this->log = [];
        }

        return $result;
    }
}
