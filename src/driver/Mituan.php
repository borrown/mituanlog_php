<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

namespace think\log\driver;

use think\App;

/**
 * 本地化调试输出到文件
 */
class Mituan extends File
{
    /**
     * CLI日志解析
     * @access protected
     * @param  array     $info 日志信息
     * @return string
     */
    protected function parseCliLog($info)
    {
        if ($this->config['json']) {
            $message = json_encode($info, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES) . "\r\n";
        } else {
            $now = $info['timestamp'];
            unset($info['timestamp']);
            $message = "{$now} ";
            $type = $msg = '';

            if (isset($info['type'])) {
                $type = $info['type'];
                $msg  = $info['msg'];
            } else {
                $type = current(array_keys($info));
                $msg  = $info[$type];
                if (is_array($msg)) {
                    $msg = implode(' ', $msg);
                }
            }

            $message .= "[" . strtoupper($type) . "] ";
            $message .= "(" . $this->getReference() . ") ";
            switch (strtoupper($type)) {
                case 'ACCE':
                    break;
                default:
                    $message .= $this->businessLogInfo();
                    break;
            }

            $message .= $msg;
            $message = trim(preg_replace('/\s+/', ' ', $message));
            $message .= "\r\n";
        }

        return $message;
    }

    protected function getReference()
    {
        return '-'; //trace request, not needed yet
    }

    protected function businessLogInfo()
    {
        $headers                = app('request')->header();
        $output                 = [];
        if (!isset($headers['mikro_req_id'])) {
            $headers['mikro_req_id'] = md5(uniqid('', true));
            app('request')->withHeader($headers);
        }
        $output['request_id']   = $headers['mikro_req_id'];
        $mikroSeq               = isset($headers['mikro_seq'])?$headers['mikro_seq']:0;
        $output['secuence']     = $mikroSeq + 1;

        $staticResponse = app('response');
        $userID    = property_exists($staticResponse, 'user_id')?$staticResponse->user_id:'-';
        $userType  = property_exists($staticResponse, 'user_type')?$staticResponse->user_type:'2';

        $output['user_id']       = $userID?:'-';
        $output['client_req_id'] = isset($headers['client_req_id'])?$headers['client_req_id']:'-';

        $strOutput = sprintf("<%s:%s:%s:%s> ",
                            $output['request_id'],
                            $output['secuence'],
                            $output['user_id'],
                            $output['client_req_id']
                        );

        return $strOutput;
    }

    public function save(array $log = [], $append = false)
    {
        $destination = $this->getMasterLogFile();

        $info = [];

        foreach ($log as $type => $val) {

            foreach ($val as $msg) {
                if (!is_string($msg)) {
                    $msg = var_export($msg, true);
                }

                $info[$type][] = $msg;
            }

            if (!$this->config['json'] && (true === $this->config['apart_level'] || in_array($type, $this->config['apart_level']))) {
                // 独立记录的日志级别
                $filename = $this->getApartLevelFile('', $type);

                $this->write($info, $filename, true, $append);

                unset($info[$type]);
            }
        }

        if ($info) {
            return $this->write($info, $destination, false, $append);
        }

        return true;
    }

    protected function write($message, $destination, $apart = false, $append = false)
    {
        // 检测日志文件大小，超过配置大小则备份日志文件重新生成
        $this->checkLogSize($destination);

        // 日志信息封装
        $info['timestamp'] = date($this->config['time_format']);

        foreach ($message as $type => $msg) {
            $msg = is_array($msg) ? implode("\r\n", $msg) : $msg;
            if (PHP_SAPI == 'cli') {
                $info['msg']  = $msg;
                $info['type'] = $type;
            } else {
                $info[$type] = $msg;
            }
        }

        if (PHP_SAPI == 'cli') {
            $message = $this->parseCliLog($info);
        } else {
            // 添加调试日志
            $this->getDebugLog($info, $append, $apart);

            $message = $this->parseLog($info);
        }

        $task = function($serv, $task_id, $data) use ($message, $destination) {
            error_log($message, 3, $destination);
        };

        return \think\swoole\facade\Task::async($task);
    }


    /**
     * 解析日志
     * @access protected
     * @param  array     $info 日志信息
     * @return string
     */
    protected function parseLog($info)
    {
        $requestInfo = [
            'ip'     => $this->app['request']->ip(),
            'method' => $this->app['request']->method(),
            'host'   => $this->app['request']->host(),
            'uri'    => $this->app['request']->url(),
        ];

        if ($this->config['json']) {
            $info = $requestInfo + $info;
            return json_encode($info, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES) . "\r\n";
        }

        array_unshift($info, "---------------------------------------------------------------\r\n[{$info['timestamp']}] {$requestInfo['ip']} {$requestInfo['method']} {$requestInfo['host']}{$requestInfo['uri']}");
        unset($info['timestamp']);

        return implode("\r\n", $info) . "\r\n";
    }

    protected function getDebugLog(&$info, $append, $apart)
    {
        if ($this->app->isDebug() && $append) {

            if ($this->config['json']) {
                // 获取基本信息
                $runtime = round(microtime(true) - $this->app->getBeginTime(), 10);
                $reqs    = $runtime > 0 ? number_format(1 / $runtime, 2) : '∞';

                $memory_use = number_format((memory_get_usage() - $this->app->getBeginMem()) / 1024, 2);

                $info = [
                    'runtime' => number_format($runtime, 6) . 's',
                    'reqs'    => $reqs . 'req/s',
                    'memory'  => $memory_use . 'kb',
                    'file'    => count(get_included_files()),
                ] + $info;

            } elseif (!$apart) {
                // 增加额外的调试信息
                $runtime = round(microtime(true) - $this->app->getBeginTime(), 10);
                $reqs    = $runtime > 0 ? number_format(1 / $runtime, 2) : '∞';

                $memory_use = number_format((memory_get_usage() - $this->app->getBeginMem()) / 1024, 2);

                $time_str   = '[运行时间：' . number_format($runtime, 6) . 's] [吞吐率：' . $reqs . 'req/s]';
                $memory_str = ' [内存消耗：' . $memory_use . 'kb]';
                $file_load  = ' [文件加载：' . count(get_included_files()) . ']';

                array_unshift($info, $time_str . $memory_str . $file_load);
            }
        }
    }

    protected function getMasterLogFile()
    {
        $host = env('hostname');

        if ($this->config['max_files']) {
            $files = glob($this->config['path'] . '*.log');

            try {
                if (count($files) > $this->config['max_files']) {
                    unlink($files[0]);
                }
            } catch (\Exception $e) {
            }
        }

        $cli = PHP_SAPI == 'cli' ? '_cli' : '';

        if ($this->config['single']) {
            $name = is_string($this->config['single']) ? $this->config['single'] : 'single';

            $destination = $this->config['path'] . 'biz' . DIRECTORY_SEPARATOR . $name . "_" . $host . $cli . '.log';
        } else {
            $filename = date('Ymd') . "_" . $host . $cli . '.log';

            $destination = $this->config['path'] . 'biz' . DIRECTORY_SEPARATOR . $filename;
        }

        $path = dirname($destination);
        !is_dir($path) && mkdir($path, 0755, true);

        return $destination;
    }

    protected function getApartLevelFile($path, $type)
    {
        $cli = PHP_SAPI == 'cli' ? '_cli' : '';
        // $host = getHostByName(getHostName());
        $host = env('hostname');

        if ($this->config['single']) {
            $name = is_string($this->config['single']) ? $this->config['single'] : 'single';
        } else {
            $name = date('Ymd');
        }

        $destination =  $this->config['path'] . DIRECTORY_SEPARATOR . strtolower($type) . DIRECTORY_SEPARATOR . $name . '_' . $host . $cli . '.log';

        $path = dirname($destination);

        !is_dir($path) && mkdir($path, 0755, true);

        return $destination;
    }
}

